# Evolvere Solarized Light Pure Aurorae Theme

Fork of Evolvere Light Pure Aurorae Theme (https://www.opendesktop.org/p/1002632/) with Solarized Light color and smaller top bar.

![screenshot](EvolvereSolarizedLightPure.png)